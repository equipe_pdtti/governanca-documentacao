## Equipe Técnica

# Presidência
Ernesto Lozardo

# Diretoria de Estudos e Políticas Regionais, Urbanas e Ambientais
Alexandre Xavier Ywata de Carvalho – Diretor
Adolfo Sachsida – Diretor adjunto

# Coordenação de Estudos em Desenvolvimento Urbano
Constantino Cronemberger Mendes

# Coordenação Técnica do Projeto Governança Metropolitana no Brasil

Bárbara Oliveira Marguti 

Marco Aurélio Costa 

Vanessa Gapriotti Nadalin 

# Equipe técnica

Carlos Vinícius da Silva Pinto 

Cesar Buno Favarão 

Clayton Gurgel de Albuquerque 

Sara Rebello Tavares 

# Desenvolvimento da Plataforma Web

Breno Rodrigues Cardoso de Moura

Bruno Cristiano Rangel dos Santos

Carlos Eduardo Gregório (Webdesign)

Ennio Ferreira Bastos Junior

Gustavo Gomes Basso

Gustavo Maia Rodrigues Gomes

Michel Junio Ferreira Rosa

Rodrigo Marques dos Santos
