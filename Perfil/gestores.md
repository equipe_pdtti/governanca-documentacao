# RM Natal
## cod=2401

### Gustavo Nogueira - Secretário de Estado de Planejamento e das Finanças 

# RM Belém
## cod=1501

### Helena Tourinho - Diretora de Desenvolvimento Metropolitano na Secretária de Desenvolvimento Urbano e Obras Públicas do Estado do Pará

# RM Belo Horizonte
## cod=3101

### Flávia Mourão Parreira do Amaral - Diretora Geral Agência RMBH

# RM Curitiba
## cod=4101

### Carlos Do Rego Almeida Filho - Coordenador Geral da Região Metropolitana de Curitiba 

# RIDE DF
## cod=53

### Antônio Carlos Nantes de Oliveira - Superintendente da SUDECO

# RM Florianópolis 
## cod=4201

### Cássio Taniguchi - Superintendente da SUDERF

# RM Fortaleza
## cod=2301

### George Valentim - Presidente do Inesp

# RM Goiânia
## cod=5201

### Everton Chaves Correia - Subsecretario de Assuntos Metropolitanos, Cidades, Infraestrutura, e Comércio Exterior

# RM São Luís
## cod=2101

### Livio Jonas Mendonça Correa - Presidente da Agência Exeecutiva Metropolitana (AGEM-MA)

# RM da Grande Vitória
## cod=3201

### Luiz Paulo Vellozo Lucas - Diretor Presidente do Instituto Jones dos Santos Neves


# RM Porto Alegre
## cod=4301

### Pedro Bisch Neto - Diretor Superintendente da Metroplan

# RM Recife
## cod=2601

### Bruno de Moraes Lisboa - Presidente CONDEPE/FIDEM

# RM Rio de Janeiro
## cod=3301

### Luis Fernando Panelli Cesar - Diretor Executivo de Gestão Metropolitana 

# RM Salvador
## cod=2901

### Fernando Torres - Secretário de Desenvolvimento Urbano no Estado da Bahia

# RM São Paulo
## cod=3501

### Bruno Covas - Prefeito do Município de São Paulo, presidente CDRMSP

# RM Vale do Rio Cuiabá
## cod=5101

### Maristene Amaral Matos - Presidente da Agência Metropolitana do Vale do Rio Cuiabá Agem/VRC

# RM Manaus
## cod=1301

### Fabiano Lopes da Silva - Coordenador Executivo da Fundação Vitória Amazônica - FVA/ORMM