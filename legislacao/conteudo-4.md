[![](imgs/legisl_img_4.jpg?raw=true)](https://gitlab.com/equipe_pdtti/governanca-documentacao/tree/master/legislacao/leis%20complementares)

# Leis Complementares: institucionalização das Regiões Metropolitanas brasileiras

O Instituto de Pesquisa Econômica Aplicada realiza o monitoramento do quadro institucional das RMs brasileiras. Nesse espaço disponibilizamos as leis complementares do que hoje representa o universo metropolitano oficial (institucionalizado), ou seja, o conjunto de RMs cuja existência está definida por lei federal ou estadual até 31 de janeiro de 2018. 


