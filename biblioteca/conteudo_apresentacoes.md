
[Seminário Internacional PDR/ABC "Gestão e governança metropolitanas: a experiência das RMs no Brasil" (JUN/2016)](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/160616_seminaario_internacional_consoorcio_abc_marco_aurelio_costa.pdf)

[Oficina Nacional: Adequação do Arranjos de Governança Metropolitana ao Estatuto da Metrópole e PDUIs (MAI/2016)](www.ipea.gov.br/redeipea/images/downloads/apresentacoes_rms_oficina_governan%C3%A7a_metropolitana_16_17_maio_2016.zip)

[Entidade metropolitana da Região Metropolitana de Salvador (DEZ/2015)](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/160406_entidade_metropolitana_da_regiao_metropolitana_de_salvador.pdf)

[Seminário Internacional Planejamento Metropolitano” – ONU-Habitat e MCidades (DEZ/2015)](https://es.unhabitat.org/estatuto-de-la-metropoli-completa-un-ano/)

[Seminário Diálogos sobre a Questão Metropolitana no Brasil – Banco Mundial (JUN/2015)](www.ipea.gov.br/redeipea/images/downloads/bancomundial_governanca_ipea_jun2015.zip)

[XVI ENANPUR-BH (MAI/2015)](www.ipea.gov.br/redeipea/images/downloads/se_enanpur_rede.zip)

[Lançamento livros 1, 2 e 3 (AGO/2013 e NOV/2014)](www.ipea.gov.br/redeipea/images/downloads/lancamentos-livros%201-2-e-3.zip)

[Oficinas (NOV/2012 e ABR/2013)](www.ipea.gov.br/redeipea/images/downloads/oficinas-nov2012-e-abr2013.zip)

[Outras apresentações](www.ipea.gov.br/redeipea/images/downloads/outras_apresentacoes.zip)
