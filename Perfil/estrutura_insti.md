# RM Natal
## cod=2401

![natal](imgs/est-inst-natal.png?raw=true)

### id= seplann

#### coords=15,24,209,78

### id= CDMNN

#### coords= 267,25,460,75

### id=parlamenton

#### coords= 623,134,429,188

### id= mesan

#### coords=749,75,649,24

### id= colegion

#### coords=753,131,651,187

### id= colegiadopn

#### coords= 749,235,649,287


# RM Belém
## cod=1501 

![belen](imgs/est-inst-belem.png?raw=true)

### id=codem

#### coords=269,60,538,134

### id=seplan

#### coords=36,214,306,290

### id=fdn

#### coords=494,215,764,290  

# RM Belo Horizonte
## cod=3101

![bh](imgs/est-inst-belo-horizonte.png?raw=true)

### id=conselhoBH

#### coords=270,67,405,120

### id=assembleia

#### coords=634,67,769,120

### id=agem

#### coords=36,204,258,272 

### id=fdm

#### coords=417,204,639,272

# RM Curitiba
## cod=4101

![curitiba](imgs/est-ins-curitiba.png?raw=true)

### id=comec

#### coords=14,307,229,400

### id=municipal

#### coords=14,78,298,154

### id=assomec

#### coords=476,172,784,296 

# RIDE DF
## cod=53

![df](imgs/est-inst-df.png?raw=true)

### id=coaride

#### coords=246,43,517,97 

### id=secretari

#### coords=246,144,517,181

# RM Florianópolis 
## cod=4201

![florianopolis](imgs/est-inst-florianopolis.png?raw=true)

### id=suderf

#### coords=367,51,637,135

### id=coderf

#### coords=159,206,431,274

### id=colegio

#### coords=631,206,768,258

### id=superintendencia

#### coords=26,340,248,392

### id=diretoria-florianopolis

#### coords=324,340,573,392

# RM Fortaleza
## cod=2301

![fortaleza](imgs/est-inst-fortaleza.png?raw=true)

### id=cdm

#### coords=267,62,537,116 

### id=fdmFortaleza

#### coords=267,165,537,219

# RM Goiânia
## cod=5201

![goiania](imgs/est-inst-goiania-2019.png?raw=true)

### id=conselhoGoiania

#### coords=272,42,542,96

### id=secretaria

#### coords=35,127,305,165

### id=cdtc

#### coords=123,248,196,290

### id=camara

#### coords=311,248,505,318

### id=demais

#### coords=558,248,751,318

### id=cmtc

#### coords=124,312,196,355

# RM São Luís
## cod=2101

![sao luis](imgs/est-inst-sao-luiz.png?raw=true)

### id=coadegs

#### coords=267,62,537,116

# RM da Grande Vitória
## cod=3201

![vitoria](imgs/est-inst-vitoria.png?raw=true)

### id=comdevit

#### coords=269,27,539,101

### id=cagem

#### coords=62,188,284,262

### id=ijsn

#### coords=518,188,740,262

### id=cates

#### coords=34,333,205,407

### id=ge

#### coords=238,334,412,408

### id=fumdevit

#### coords=467,333,740,407

# RM Porto Alegre
## cod=4301

![porto alegre](imgs/est-ins-porto-alegre.png?raw=true)

### id=diretoria

#### coords=388,80,558,184

### id=representativas

#### coords=594,36,750,104

### id=gestao

#### coords=256,200,496,282

# RM Recife
## cod=2601

![recife](imgs/est-inst-recife.png?raw=true)

### id=conderm

#### coords=202,24,473,110

### id=conselho

#### coords=499,24,770,110

### id=funderm

#### coords=35,126,306,179

### id=camaras

#### coords=529,126,770,169

### id=funderm2

#### coords=35,195,306,249

### id=cmds

#### coords=202,273,274,315

### id=cmt

#### coords=326,273,398,315

### id=cmduot

#### coords=450,273,522,315

### id=cmmas

#### coords=574,273,646,315

### id=cmpds

#### coords=698,273,770,315

# RM Rio de Janeiro
## cod=3301

![Rj](imgs/est-inst-rio-de-janeiro_NOVO.png?raw=true)

### id=conselhoRJ

#### coords= 335,18,453,75

### id=fundo

#### coords= 225,57,315,120

### id=instituto

#### coords= 436,93,357,143

### id=consultivoRj

#### coords= 565,55,475,120

### id=assessoria

#### coords=

### id=chefia

#### coords=

### id=procuradoria

#### coords=

### id=auditoria

#### coords=

### id=relações

#### coords=

### id=planejamento

#### coords=

### id=projetos

#### coords=

### id=administrativa

#### coords=

### id=informação

#### coords=

### id=secretaExe

### coords= 678,210,583,240

# RM Salvador
## cod=2901

![salvador](imgs/est-inst-salvador.png?raw=true)

### id=colegiado

#### coords=202,31,473,105

### id=secretario

#### coords=202,126,473,180

### id=tecnico

#### coords=34,193,305,299

### id=conselhoSalvador

#### coords=367,193,638,299

### id=tematico

#### coords=34,323,305,414

### id=grupos

#### coords=367,323,638,414

# RM São Paulo
## cod=3501

![sp](imgs/est-inst-sao-paulo.png?raw=true)

### id=cdrmsp

#### coords=271,62,541,131

### id=comite

#### coords=107,207,242,260

### id=comissao

#### coords=564,207,700,260 

### id=gruposSP

#### coords=107,298,242,350

### id=emplasa

#### coords=564,298,700,365

# RM Vale do Rio Cuiabá
## cod=5101

![cuiaba](imgs/est-inst-cuiaba.png?raw=true)

### id=codem

#### coords=270,61,540,129

### id=agemCuiaba

#### coords=38,208,308,275

### id=fds

#### coords=496,208,766,275 

# RM Manaus
## cod=1301

![Manaus](imgs/est-inst-manaus_fim.png?raw=true)

### id=SRMM

#### coords=

### id=secrtexi

#### coords=

### id=UPGE

#### coords=

### id=CDSRMM

#### coords=

### id=FERMM

#### coords=