
# Créditos de Imagem

# Home

Imagem 1 - Título da reportagem: Manaus quer tirar a cidade da lista de capitais menos arborizadas do país – Foto: Jose Zamith
Disponível em: http://agenciabrasil.ebc.com.br/geral/noticia/2016-05/manaus-quer-tirar-cidade-da-lista-de-capitais-menos-arborizadas-do-pais

Imagem 2 – Título da reportagem: Salvador, a primeira capital do Brasil completa 465 anos hoje – Foto: Valter Pontes/Agecom
Disponível em: https://fotospublicas.com/salvador-primeira-capital-brasil-completa-465-anos-hoje/

Imagem 3 – Título da reportagem: Polo do Carnaval, bairro do Recife Antigo impulsionou crescimento da região - Foto: Portal Brasil
Disponível em: http://www.brasil.gov.br/cultura/2015/02/polo-do-carnaval-recife-antigo-impulsionou-desenvolvimento-da-regiao/bairro_do_recife_2-secretaria-de-turismo-prefeitura.jpg/view
