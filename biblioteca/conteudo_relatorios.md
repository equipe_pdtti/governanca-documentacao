**RM Belém** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel_1_1_rm_belem.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/160128_relatorio_rm_belem.pdf) 

**RM Belo Horizonte** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel1_1_rmbh.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151228_relatorio__rmbh.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190114_relatorio_de_pesquisa_implementacao_BH.pdf)

**RM Curitiba** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_arranjos_igm_rm_curitiba.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150930_relatorio_analise_curitiba2.pdf) 

**RM Florianópolis** - [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190506_adequacao_de_arranjos_florianopolis.pdf)

**RM Fortaleza** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150928_relatorio_arranjos_fortaleza.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151013_relatorio_analise_fortaleza2.pdf) 

**RM Grande São Luís** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel1_1_rmgsl.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_analise_comparativa_grande_sao_luis.pdf) 

**RM Grande Vitória** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_arranjo_grande_vitoria.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/62592_relatorio_rmgv.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/180219_relatorio_adequacao_dos_arranjos_de_governanca_metropolitana.pdf)

**RM Goiânia** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rmg_livro_web.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150820_74657_relatorio_analise_rm_Goiania.pdf) 

**RM Porto Alegre** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_arranjos_porto_alegre.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_analise_porto_alegre.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190118_relatorio_pesquisa_adequacao_porto_alegre.pdf) 

**RIDE DF** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/ride_livro_web.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_analise_distrito_federal.pdf) 

**RM Rio de Janeiro** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150820_relatorio_arranjos_riode_janeiro.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_analise_componente2_riode_janeiro.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/181010_relatorio_pesquisa_analise_RJ.pdf) 

**RM Recife** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150717_relatorio_arranjos_reecife.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150917_relatorio_analise_comparativa_rm_recife.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190114_relatorio_de_pesquisa_arranjo_de_governanca_recife.pdf)

**RM São Paulo** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150730_relatorio_arranjos_saopaulo.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151208_relatorio_analise_comparativa_rm_sao_paulo.pdf) - [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/20170217_relatorio_implementacao-estatuto.pdf)

**RM Salvador** - [Componente. 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/relatorio_1.1_revisao_final_salvador.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151228_relatorio_analise_salvador_2.pdf) [Componente. 3](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/180223_relatorio_implementacao_salvador.pdf)

**RM Vale do Rio Cuiabá** - [Componente 1](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150814_relatorio_arranjos_valeriocuiba.pdf) - [Componente. 2](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150917_relatorio_analise_vale_rio_cuiaba.pdf) 
