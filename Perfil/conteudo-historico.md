# Rm Natal 

## cod=2401

Localizada na orla litorânea do Nordeste brasileiro, Natal, capital do estado do Rio Grande do Norte, foi institucionalizada por meio da Lei Complementar Estadual no 152, de 16 de janeiro de 1997. Formada por catorze municípios, a Região Metropolitana de Natal (RMN) apresenta-se como uma das regiões de maior dinamismo econômico e social do Rio Grande do Norte. A RM de Natal é composta pelos seguintes municípios: Ceará-Mirim, Extremoz, São Gonçalo do Amarante, Natal, Parnamirim, Macaíba, São José do Mipibu, Monte Alegre, Nísia Floresta, Vera Cruz, Maxaranguape, Ielmo Marinho, Goianinha e Arês, sendo que os dois últimos foram incorporados em 2016.

# RM Belém
## cod=1501

A RM de Belém, composta atualmente por sete municípios (até o ano de 2010, por Belém, Ananindeua, Marituba, Benevides, Santa Bárbara do Pará e Santa Isabel do Pará e, a partir de 2011, também por Castanhal), está situada na foz do rio Pará, sendo ainda cortada por vários rios e igarapés, formando uma grande área de várzea. nalisando o valor absoluto da população da RM de Belém, ela passou de 1.973.259, no Censo 2000, para 2.275.032, no Censo 2010. Em 1973, por meio da Lei Complementar (LC) n° 14, o governo federal instituiu as RM de Belém. 

# RM Belo Horizonte
## cod=3101

A Região Metropolitana de Belo Horizonte foi criada pela Lei Federal nº 14/1973. Atualmente composta por 34 municípios, sua população no ano de 2000 era de 4.358.171 habitantes; em 2010, esse contingente passou para 4.883.970 habitantes, distribuídos num território de 9.472,6 km2, o que significou uma variação de crescimento, em relação ao ano de 2000, de 12,06%. O município mais populoso é Belo Horizonte, concentrando 48,63% da população metropolitana (2.375.151 habitantes). Durante os anos de 2007 e 2009, foi definida a necessidade de adoção de um Plano Diretor de Desenvolvimento Integrado da RMBH (PDDI-RMBH) como instrumento de planejamento. Este Plano teve como objetivo estabelecer instrumento e políticas para o desenvolvimento do território metropolitano. Nesse período também se estabeleceu a estrutura institucional. 

# RM Curitiba
## cod=4101

A RM de Curitiba foi criada pela Lei Complementar Federal (LCF) nº 14, de 8 de junho de 1973, que instituiu as primeiras RMs brasileiras Constituída por 29 municípios, a RM de Curitiba tem na capital paranaense seu núcleo, e se configura como a espacialidade mais concentradora no estado do Paraná, e muitos de seus municípios crescem mais que a média estadual, demonstrando que o dinamismo da aglomeração se estende sobre áreas cada vez mais distantes do núcleo. Atualmente, a RM de Curitiba possui a maior extensão entre as RMs brasileiras apontadas pelo estudo da Regic (IBGE, 2008), ocupando uma área de 16.627 km², que corresponde a 8% do território do Paraná e população estimada de 3.285.251, em 2012 (Ipardes, 2013), correspondente a 31% da população estadual.

# RIDE DF
## cod=530

A formação da área metropolitana de Brasília se intensificou, a partir do início da década de 1970, com a consolidação da transferência da capital. A expansão desta área ocorreu de forma polinucleada e esparsa no território do DF (Paviani, 2010), perpassando seus limites político-administrativos e abrangendo um espaço de influência direta em municípios do estado de Goiás, formando um aglomerado urbano na área metropolitana de Brasília. A configuração espacial do território constitui-se por 22 municípios. Segundo IBGE, Brasília é uma metrópole nacional e núcleo de uma área metropolitana que abrange o DF e onze municípios goianos: Águas Lindas de Goiás, Alexânia, Cidade Ocidental, Cristalina, Formosa, Luziânia, Novo Gama, Padre Bernardo, Planaltina, Santo Antônio do Descoberto, Valparaíso de Goiás.

# RM Florianópolis 
## cod=4201

A Região Metropolitana de Florianópolis – RMF foi criada pela Lei Complementar Estadual n° 162 de 1998, foi extinta pela Lei Complementar Estadual n° 381 de 2007 e reinstituída pela Lei Complementar Estadual n° 495 de 2010. Posteriormente, foi redefinida pela Lei Complementar nº 636, de 9 de setembro de 2014. Constituída por nove municípios, a população apresenta elevada concentração no município sede, onde a população de Florianópolis corresponde à cerca de 48% do montante populacional da região. A ocupação territorial da Grande Florianópolis aumenta significativamente somente a partir da década de 1970, com expansão do sistema viário tanto na porção continental quanto na insular, provocado pelos investimentos em turismo e transportes, como a melhoria do Aeroporto Hercílio Luz e a construção da Ponte Pedro Ivo Campos. 

# RM Fortaleza
## cod=2301

A RM de Fortaleza é uma macrorregião de planejamento do estado do Ceará, sendo composta por quinze municípios, que cobrem uma área de 5.783,60 m² (3,89% do estado), com uma população de 3.615.767 domiciliados (42,8% do estado) e um produto interno bruto (PIB) agregado de R$ 50,6 bilhões (65% do estado). A RM de Fortaleza foi criada por força de lei – Lei Complementar Federal (LCF) no 14/1973 – no começo da década de 1970 (8 de junho de 1973), sendo composta inicialmente por cinco municípios: Fortaleza, Caucaia, Maranguape, Pacatuba e Aquiraz. O processo de evolução da composição dos municípios que formam a RM deu-se tanto por desmembramentos causados por emancipações políticas de distritos que pertenciam a estes municípios como por incorporação de novos municípios. 

# RM Goiânia
## cod=5201

Goiânia, construída na década de 1930, para abrigar a transferência da capital estadual da Cidade de Goiás (Goiás Velho), e impactada com a transferência da capital federal para Brasília, viveu um acelerado processo de parcelamento nas décadas de 1950 e 1960, impulsionando a explosão demográfica para os municípios do entorno, e ainda em franca expansão. Criada pela LCE nº 27/1999, a Região Metropolitana de Goiânia viu seu contingente populacional crescer a uma taxa geométrica de 2,23%, entre 2000 e 2010: passando de 1,743 milhão de habitantes, para 2,173 milhões de habitantes; distribuídos em um território de 7.315,1 km² (densidade demográfica de 297,07 hab./km²) e taxa de urbanização de 98%. Em 2019, conforme a estimativa do IBGE, e após alteração da composição do aglomerado pela LCE nº 149/2019 (totalizando 21 municípios), o contingente populacional metropolitano da RMG alcançou 2,613 milhões de habitantes. 

# RM São Luís
## cod=2101

A RMGSL, formada pelos municípios de Alcântara, Paço do Lumiar, Raposa, São José de 
Ribamar e São Luís, a capital do estado, segundo dados do IBGE, com base no Censo de 2010, a RMGSL reúne 1.331.181 habitantes, concentrando 20,25% da população total dos 217 municípios maranhenses, e possui um PIB, a preços correntes, registrado em 2009, de R$ 16,269 bilhões. A RMGSL apresenta uma densidade demográfica de 459,2 hab./km² (IBGE, 2010), e seus espaços estão sendo historicamente ocupados, desconsiderando-se as vulnerabilidades e potencialidades ambientais para ordenar o território metropolitano; são exemplos as extensas áreas de mangues que foram suprimidas.

# RM da Grande Vitória
## cod=3201

Por meio da Lei Complementar nº 58/1995 foi instituída a Região Metropolitana da Grande Vitória (RMGV), com vista à organização, ao planejamento e à execução de funções públicas de interesse comum, no âmbito metropolitano. Constituída originalmente pelos municípios de Cariacica, Serra, Viana, Vila Velha e Vitória, sendo incluídos nos anos seguintes, por meio das Leis Complementares nº 159/1999 e n°204/2001, os municípios de Guarapari e Fundão, respectivamente.

# RM Porto Alegre
## cod=4301

A Região Metropolitana de Porto Alegre foi criada pela Lei Federal nº 14/1973. Em 1973 eram 14 municípios, com a CF 1988 foram incorporados mais 8 municípios, chegando a 22. Com as incorporações dos anos 1990 e 2000, em 2015 a RM alcançou 34 municípios. A RMPA compreende uma área territorial de 10.346,21 km² - mais do que o dobro do tamanho da RM inicial em 1973. Ao longo dos mais de 40 anos de sua existência a RM de Porto Alegre passou de 1,5 milhões de habitantes, em 1973, para em torno de quatro milhões de habitantes em 2010, com crescimento da população no período 1973-2015 de 163,3%. Atualmente a densidade demográfica é de 390 hab/km². Em 2010, a metade dos municípios metropolitanos registrou crescimento populacional de até 0,98% ao ano. 

# RM Recife
## cod=2601

A RM do Recife foi criada pela Lei Complementar Federal n 14, de 8 de junho 1973. Segundo IBGE, 2010, a RM do Recife possui uma população de 3,69 milhões de pessoas que vivem nos quinze municípios. A expansão da economia pernambucana foi influenciada, sobretudo, pelo desempenho do setor industrial, em especial a atividade da construção civil, seguido da indústria de transformação - repercussões do ciclo de desenvolvimento ocorrido na economia brasileira desde os anos iniciais do século XXI. Este impulso levou à expansão da indústria de bens de consumo duráveis, serviços e comércio, sobretudo a ampliação da infraestrutura e a implantação de novos grandes empreendimentos no entorno metropolitano e a ampliação de projetos já existentes. Em um raio de 300 km a partir de Suape, que tem seu entorno mais imediato à RM do Recife. Esse raio envolve um mercado consumidor de 12 milhões de pessoas (IBGE, Censo 2010) e 35% do PIB o Nordeste, quatro capitais, dois aeroportos internacionais e três regionais e cinco portos internacionais.

# RM Rio de Janeiro
## cod=3301

No ano seguinte a lei Complementar Federal 14/1973, que cria as primeiras Regiões Metropolitanas Brasileiras, chega à vez do Rio de Janeiro, que assiste à criação de sua região metropolitana no mesmo momento em que se dá a fusão dos estados da Guanabara - composto pelo então Distrito Federal, situado na cidade do Rio de Janeiro. Desde então a Região Metropolitana do Rio de Janeiro (RMRJ) já teve diferentes configurações, tanto por conta da inclusão/exclusão de municípios quanto por conta das emancipações ocorridas desde então em seu território. Atualmente são 22 municípios, após o ingresso de dois municípios no final do ano de 2013 e mais um em 2018 - Petrópolis. Segundo maior núcleo urbano do Brasil, a metrópole do Rio de Janeiro reúne mais de 12 milhões de habitantes (IPEA, 2015) e concentra cerca de 75% da população do estado do Rio de Janeiro, concentrando a maior proporção populacional RM/Estado do país.

# RM Salvador
## cod=2901

A Região Metropolitana de Salvador foi criada entre as primeiras do país por legislação federal, através da Lei Complementar nº 14 de 1973, e hoje ocupa uma área de 4.375,123 km², inserida no bioma de mata atlântica, abrigando uma população de 3.574.804 habitantes (IBGE, 2010), correspondendo a 25,5% da população do Estado da Bahia. Hoje é formada por 13 municípios, sendo eles: Salvador, Lauro de Freitas, Simões Filho, Camaçari, Dias D’Ávila, Mata de São João, Candeias, Madre de Deus, Itaparica, Vera Cruz, São Sebastião do Passé, São Francisco do Conde e Pojuca. A RMS possui o maior PIB da Bahia, respondendo por 53,7% do PIB Estadual, representando um dos seis mais importantes mercados regionais do Brasil. Salvador e sua Região Metropolitana apresentaram, nas últimas décadas, taxas de crescimento do PIB superiores às duas principais metrópoles do país São Paulo e Rio de Janeiro. 

# RM São Paulo
## cod=3501

A RM de São Paulo foi instituída primeiramente por regulação federal na década de 1970 (Lei Complementar Federal no 14/1973). A RM de São Paulo é um dos maiores polos econômicos do país, oferece um leque diversificado de oportunidades geradoras de empregos, resultando na maior concentração populacional brasileira. Em termos populacionais, a região abriga 19,9 milhões de habitantes (Fseade, 2012). O produto interno bruto (PIB) da RM de São Paulo de 2010 – R$ 701,85 bilhões – equivale a cerca de 56% do PIB do estado e 20% do PIB do Brasil, sendo responsável pelo recolhimento de um quarto dos impostos no país (Fseade, 2012). A RM de São Paulo ocupa uma área de 7.943 km2, menos de um milésimo da superfície nacional e pouco mais de 3% do território paulista. As dinâmicas sócio-econômicas da região metropolitana e, em especial, a experiência urbana de quem habita a metrópole e percebe o problema metropolitano, passam a exigir o diálogo e a pactuação de diretrizes políticas entre as diversas entidades federativas que compõe a Região Metropolitana de São Paulo para o desenvolvimento do território baixo posturas convergentes entre as entidades públicas que representam a gestão do território metropolitano que conta com o Governo do Estado de São Paulo, 39 municípios e 5 consórcios públicos interfederativos.

# RM Vale do Rio Cuiabá
## cod=5101

A Região Metropolitana do Vale do Rio Cuiabá, constituída pela Lei Complementar (LC) n° 359, de 27 de maio de 2009 que posteriormente foi alterada pela LC 577/2016, que inclui na RMVRC os municípios de Acorizal e Chapada dos Guimarães, a qual passou a ser composta por seis municípios (Acorizal, Chapada dos Guimarães, Cuiabá, Nossa Senhora do Livramento, Santo Antônio de Leverger e Várzea Grande). Na expressiva mancha urbana de Mato Grosso, a problemática demográfica evidencia não somente o contingente populacional e os movimentos migratórios, mas também as intrincadas disparidades nos indicadores sociais. Enquanto Cuiabá e Várzea Grande são municípios com mais de 96% da população morando em área urbana, Nossa Senhora do Livramento, Acorizal e Santo Antônio do Leverger apresentam a menor população e as menores taxas de urbanização e de Índice de Desenvolvimento Humano Municipal (IDH-M), abaixo da média estadual (0,773) e nacional (0,766). Quanto à participação da RM do Vale do Rio Cuiabá na composição do Valor Adicionado Bruto (VAB) do estado, verifica-se que esse montante chega a 21,7%, sendo bastante elevada a porção no setor industrial (49,4%), seguido do de serviços (27,0%), enquanto o da agropecuária representa apenas 1,5% (IPEA, 2013). 

# RM Manaus
## cod=1301

A Região Metropolitana de Manaus é uma da RMs mais singular do Brasil devido ao seu enorme território. A RMM é composta por 13 municípios e possui mais de 100 mil Km² apesar de possuir a menor densidade demográfica do país com somente 20 habitantes por Km². Dos municípios que compõem a RM, apenas Manaus possui uma urbanização consolidada, concentrando 85% de toda a população da região. Os municípios que compõem a região são: Autazes, Careiro, Careiro da Várzea, Iranduba, Itacoatiara, Itapiranga, Manacapuru, Manaquiri, Manaus, Novo Airao, Presidente Figueiredo, Rio Preto da Eva e Silves. Sabendo que o Estado do Amazonas possui uma extensa floresta tropical e ampliação das taxas de desmatamento, a RMM perdeu quase 137 mil hectares de floresta de 1998 a 2013. Desde 2007, quando a RMM foi criada, foram perdidos quase 36 mil hectares  de floresta.    





