cod=2401

**RM Natal** 

Status: sem informação 

cod=1501

**RM Belém** 

Status: sem informação

cod=3101

**RM Belo Horizonte**

[Status: Concluído](http://www.agenciarmbh.mg.gov.br/)

cod=4101

**RM Curitiba**

Status: sem previsão

cod=4201

**RM Florianópolis**

Status: Em elaboração

cod=2301

**RM Fortaleza**

Status: sem previsão 

cod=2101

**RM Grande São Luís**

Status: sem informação

cod=3201

**RM Grande Vitória**

[Status: Sancionado](https://planometropolitano.es.gov.br/)

cod=5201

**RM Goiânia**

[Status: Concluído](http://pdi-rmg.secima.go.gov.br/)

cod=4301

**RM Porto Alegre**

Status: sem informação

cod=53

**RIDE DF** - 

Status: sem previsão

cod=3301

**RM Rio de Janeiro**

[Status: Concluído](http://www.camarametropolitana.rj.gov.br/)

cod=2601

**RM Recife** - 

Status: sem previsão

cod=3501

**RM São Paulo** - 

[Status: Concluído e aprovado pelo CDRMSP](https://www.pdui.sp.gov.br/rmsp/)

cod=2901

**RM Salvador**

Status: sem informação

cod=5101

**RM Vale do Rio Cuiabá**

[Status: Sancionado](http://www.pddivrc.ibam.org.br/)

cod=1301

**RM Manaus** 

Status: sem informação