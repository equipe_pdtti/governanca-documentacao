[Adequação dos Arranjos de Governança Metropolitana ao Estatuto da Metrópole e PDUIs (MAIO/2016)](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/160729_ipea_memoria_oficina_nacional_governanca_metropolitana_mai_2016.pdf)

[Seminário e Oficina Política Metropolitana: Governança, Instrumentos e Planejamento Metropolitanos (SET/2017)](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/biblioteca/PDFs/Press%20release%20I%20Semin%C3%A1rio%20e%20Oficina%20Pol%C3%ADtica%20Metropolitana.pdf)

[Seminário e Oficina Política Metropolitana: Governança, Instrumentos e Planejamento Metropolitanos (ABRIL/2018)](https://gitlab.com/equipe_pdtti/governanca-documentacao/blob/master/biblioteca/conteudo_memoriaEvento_midias.md)