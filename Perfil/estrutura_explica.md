# RM Natal 

## ID= seplann

### Secretaria do Estado de Planejamento e Finanças (Seplan)

A Secretaria de Estado do Planejamento e das Finanças é um órgão da Administração Direta do Governo do Estado do Rio Grande do Norte. A SEPLAN tem a responsabilidade de planejar, coordenar, executar, supervisionar, controlar e avaliar os sistemas estaduais de Planejamento, Orçamento e Finanças.

## ID= CDMNN

### Conselho de Desenvolvimento Metropolitano de Natal (CDMN)

Secretaria executiva no âmbito do governo estadual, com a função de prover a administração metropolitana de instrumentos de apoio e intervenção em nível técnico e articular as ações verticalizadas (secretarias de igual natureza às dos municípios e setorializadas – secretarias e institutos) objetivando a implantação de trabalhos de competência do CDMN. 

## ID=parlamenton

### Parlamento Comum da RM de Natal

O Parlamento Comum da RM de Natal foi criado pela Resolução no 304/2001, aprovada pela Câmara Municipal de Natal (e câmeras de vereadores dos demais municípios), como um organismo de caráter eminentemente político, uma vez que não se sobrepõe às casas legislativas dos municípios que o integram. 

## ID= mesan

### Mesa Diretora 

Possui caráter de execução das decisões do parlamento, é composta por nove membros, sendo seis titulares e três suplentes.

## ID= colegion

### Colégio de Comissões 

Uma instância intermediária, composta por trinta membros, representa as respectivas câmaras municipais, provisoriamente por meio de comissões que são nomeadas por cada casa legislativa.

## ID= colegiadopn

### Colegiado Pleno 

Composto pelos vereadores dos municípios da região metropolitana.

## ID= camaratn

### Câmaras Temáticas

XXX

# RM Belém

## ID=codem

### Companhia de Desenvolvimento da Área Metropolitana de Belém (Codem)

Criado pela prefeitura de Belém, em 1970, pela Lei Municipal n° 6.795/1970, a Companhia, cuja finalidade era o ordenamento sociopolítico e econômico da área metropolitana, a ser realizado por meio da elaboração obrigatória de planos e projetos pautados no relacionamento entre poderes estadual e federal. 

## ID=seplan

### Secretaria de Estado do Planejamento e Coordenação Geral – Seplan 

Criada após 1976 a Seplan assumiu definitivamente a coordenação da gestão metropolitana, mantendo a Codem como seu braço executivo, ligado as decisões e ações imanadas da Secretária.

## ID=fdn

### Fundo de Desenvolvimento Metropolitano (FDM)

O PLC n° 003/2009 também contempla a criação do Fundo de Desenvolvimento Metropolitano (FDM), com recursos em torno de 1% da cota-parte do Fundo de Participação dos Municípios (FPE), que seria repassado a esses municípios, 1% da arrecadação do ICMS, mais 2% do total arrecadado pelo estado sobre o IPVA, além de orçamentos da União, estado e municípios envolvidos na questão metropolitana. 

# RM São Luís

## ID=coadegs

### Conselho metropolitano - COADEGS

A composição do COADEGS disciplina a participação dos Poderes Executivos e Legislativos estaduais e municipais. A organização do conselho, com base na lei vigente, é apresentada com dezoito membros, da seguinte forma: prefeitos dos municípios e presidentes das Câmaras Municipais; governador do estado do Maranhão; gerente de Estado de Planejamento, Orçamento e Gestão; e secretário municipal indicado pelo prefeito de cada município; e atribuições como: promover a elaboração, coordenar, acompanhar e controlar o Plano de Desenvolvimento Integrado; orientar, controlar e coordenar a execução de FPICs; disciplinar a aplicação dos recursos do Fundo de Desenvolvimento da RMGSL; prestar assistência técnica aos municípios; propor ao estado e aos municípios alterações tributárias extrafiscais; e contratar empresas para a implementação dos serviços públicos de interesse comum.

# RM Fortaleza

## ID=cdm

### Conselho Deliberativo - CDM 

Criado pela LC n° 18/1999, o CDM foi à última instância de gestão estadual com foco nesta região, estando inicialmente vinculado à Seinfra. O CDM tinha o objetivo de estimular a ação integrada dos agentes públicos envolvidos na execução das funções de interesse comum no âmbito metropolitano, para assegurar a eficiência no processo de desenvolvimento da RM. Entre as suas atribuições específicas, está aprovar o Plano Diretor de Desenvolvimento Urbano - PDDU da RMF e todos os demais planos, programas e projetos indispensáveis à execução das funções públicas de interesse comum metropolitano; definir as atividades, empreendimentos e serviços admitidos como funções de interesse comum metropolitano;
criar Câmaras Técnicas Setoriais, estabelecendo suas atribuições e competências; elaborar seu regimento interno.

## ID=fdm_Fo

### Fundo de Desenvolvimento - FDM

Principal instrumento de realização de serviços para as Fpics, compreendendo entre estes: atividades de planejamento de desenvolvimento da RM de Fortaleza; gestão de negócios relativos à RM de Fortaleza; execução de Fpics no âmbito metropolitano; execução e operação de serviços urbanos de interesse metropolitano; execução e manutenção de obras e serviços de interesse da RM de Fortaleza; e elaboração de planos e projetos de interesse metropolitano. O fundo, no entanto, nunca foi alimentado, mesmo tendo a possibilidade de captação em várias fontes de recursos como estabelece a LCE n°18/1999.

# RM Cuiabá

## ID=codem

### Conselho Deliberativo Metropolitano da Região do Vale do Rio Cuiabá – Codem/VRC

O Codem é tem objetivo de orientar o planejamento e a gestão integrada das funções públicas de interesse comum, no âmbito da região metropolitana, que contou com representantes do governo do estado, dos municípios e de entidades civis. 

## ID=agemCuiaba

### Agência de Desenvolvimento da Região Metropolitana do Vale do Rio Cuibá – Agem/VRC

Agem/VRC é uma entidade autárquica, componente da Administração Pública Indireta, com autonomia administrativa e financeira e prazo de duração indeterminado. Tem a missão de assegurar a execução de planos, programas e projetos relacionados às funções públicas de interesse comum no âmbito da Região Metropolitana do Vale do Rio Cuiabá – RMVRC, e tem como finalidade integrar a organização, o planejamento e a execução das funções públicas de interesse comuns direcionadas ao desenvolvimento integrado da Região Metropolitana do Vale do Rio Cuiabá – RMVRC.  Fonte: LC 499/2013

## ID=fds

### Fundo de Desenvolvimento da Região Metropolitana do Vale do Rio Cuiabá – FDM/VRC

Instrumento financeiro com a atribuição de alocar recursos e financiar planos e projetos no âmbito da região metropolitana.

# RIDE Distrito Federal

## ID=coaride

### Conselho de administração - Coaride

Órgão colegiado, formado por treze membros assim distribuídos: ministro de Estado da Integração Nacional, que o presidirá; diretor-superintendente da Sudeco; um representante do MPOG, do MF e do MCid; um representante da Casa Civil da Presidência da República, indicado por seu titular; dois representantes do MI, indicados por seu titular; um representante da Sudeco, indicado por seu titular; um representante do DF, um do estado de Goiás e um do estado de Minas Gerais, indicados pelos respectivos governadores; e um representante dos municípios que integram a Ride, indicado pelos respectivos prefeitos.

## ID=secretari

### Secretaria-Executiva

O Coaride possui uma Secretaria-Executiva, exercida pela Diretoria de Implementação de Programas e de Gestão de Fundos da Sudeco. As representações municipais e estaduais têm mandato de dois anos, podendo ser reconduzidos. Não há prazo para a renovação de mandato da representação federal no conselho. As decisões são tomadas por maioria simples de seus membros, cabendo ao seu presidente o voto de qualidade. São, portanto, nove membros do governo federal, três dos governos estaduais e apenas uma representação municipal. Nota-se grande prevalência da esfera federal na composição do conselho, justificada pelo próprio caráter interfederativo da Ride, agregando Unidades da Federação (UFs) que podem possuir interesses administrativos divergentes nesse território, cabendo à União o papel de harmonização das políticas conflitivas levadas a esse conselho.

# RM Belo Horizonte

## ID=conselhoBH

### Conselho Deliberativo

Ao Conselho Deliberativo compete deliberar sobre a compatibilização de recursos de distintas fontes de financiamento destinados à implementação de projetos indicados no Plano Diretor de Desenvolvimento Integrado; acompanhar e avaliar a execução do Plano Diretor de Desenvolvimento Integrado, bem como aprovar as modificações que se fizerem necessárias à sua correta implementação; orientar, planejar, coordenar e controlar a execução de funções públicas de interesse comum; aprovar os relatórios semestrais de avaliação de execução do Plano Diretor de Desenvolvimento Integrado e de seus respectivos programas e projetos. É no Conselho Deliberativo que integrantes da sociedade civil organizada participam da governança.  

## ID=assembleia

### Assembleia Metropolitana 

A Assembleia Metropolitana é o órgão de decisão superior e de representação do Estado e dos Municípios na região metropolitana, tem por finalidades definir as macro diretrizes do planejamento, e vetar ou não as resoluções emitidas pelo Conselho Deliberativo. Segundo a Lei Complementar nº 88 de 12/01/2009 as deliberações e resoluções da Assembleia serão aprovadas pelo voto de dois terços de seus membros. Portanto, a Assembleia engloba os representantes das prefeituras dos municípios constituintes da RMBH.

## ID=agem

### Agência de Desenvolvimento Metropolitano – AGEM

Vinculada diretamente ao Conselho Deliberativo, a Agência RMBH é uma autarquia de caráter técnico e executivo, para fins de planejamento, assessoramento, regulação urbana e apoio a execução das funções públicas de interesse comum. São atribuições da AGEM realizar estudos técnicos e diagnósticos, promover a execução das metas e prioridades estabelecidas pelo Plano Direto de Desenvolvimento Integrado (PDUI), elaborar, propor e implementar o PDUI bem como programas e projetos estabelecidos, articular-se com instituições públicas e privadas, articular-se com os municípios integrantes da RM, assistir tecnicamente os municípios, fornecer suporte técnico e administrativo à Assembleia Metropolitana e ao Conselho Deliberativo manter permanente avaliação e fiscalização da execução dos planos e projetos aprovados. 

## ID=fdm

### Fundo de Desenvolvimento Metropolitano - FDM

Criado em 2007, o Fundo de Desenvolvimento Metropolitano, tem como objetivos o financiamento da implantação de programas e projetos estruturantes e a realização de investimentos relacionados a funções públicas de interesse comum nas Regiões Metropolitanas do Estado, conforme diretrizes estabelecidas pelo Plano Diretor de Desenvolvimento Integrado de cada região metropolitana mineira.  

# RM Rio de Janeiro

## ID=conselhoRJ

O conselho deliberativo é composto pelos Prefeitos e presidido pelo Governador, conta ainda com 3 representantes da sociedade civil. Foi criado pela Lei Complementar 184/2018 e já foi implantado tendo realizado a sua 1ª Reunião Ordinária em 27 de fevereiro de 2019.

## ID=fundo

O Fundo foi criado pela Lei Complementar nº 184/2018, é um fundo orçamentário especial com o objetivo fomentar a implantação de programas e projetos estruturantes e investimentos relacionados às funções públicas de interesse comum na Região Metropolitana do Rio de Janeiro, em consonância com o Plano Estratégico de Desenvolvimento Urbano Integrado da Região Metropolitana, e integra os mecanismos institucionais da Região Metropolitana do Rio de Janeiro. 

## ID=instituto

O Instituto da Região Metropolitana do Rio de Janeiro - Instituto Rio Metrópole – atende como órgão executivo criado como entidade integrante, para fins organizacionais, da Administração Pública Estadual indireta. Na forma de uma Autarquia Especial, tem o objetivo de executar as decisões tomadas pelo Conselho Deliberativo e assegurar suporte às suas atribuições, além de gerir o Fundo de Desenvolvimento da Região Metropolitana.

## ID=consultivoRj

O Conselho Consultivo é composto por 47 membros, inclui a defensoria e o MP e com 9 membros de cada um dos segmentos identificados – Executivo, legislativo, empresarial, sociedade organizada/entidades e representantes da sociedade civil -  com o objetivo de assegurar a participação da população no processo de planejamento e tomada de decisões, bem como no acompanhamento da execução de serviços e atividades relacionadas às funções públicas de interesse comum. Não têm direito à voto, mas têm voz, através do seu presidente, nas reuniões do Conselho deliberativo, e elegem os três membros da sociedade no Conselho Deliberativo.

## ID=secretaExe

O Conselho Deliberativo disporá de uma Secretária Executiva para agendar, convocar, organizar e secretarias as reuniões do Conselho Deliberativo. 

## ID=assessoria

Nulo

## ID=chefia

Nulo

## ID=procuradoria

Nulo

## ID=auditoria

Nulo

## ID=relações

Nulo

## ID=planejamento

Nulo

## ID=projetos

Nulo

## ID=administrativa

Nulo

## ID=informação

Nulo

# RM São Paulo

## ID=cdrmsp

### Conselho de Desenvolvimento da Região Metropolitana de São Paulo (CDRMSP)

De caráter normativo e deliberativo, o CDRMSP é composto por representantes do governo do estado e de cada um dos municípios integrantes da RM de São Paulo. A representação destes entes se manifesta na seguinte proporção: o CDRMSP é composto por 39 representantes dos municípios da RMSP e 17 representantes do Governo do Estado de São Paulo, sendo que cada um dos dois setores possuem votos ponderados de modo que, no conjunto, tanto os votos do Estado, como os dos Municípios, correspondem, respectivamente, a 50% (cinquenta por cento) da votação. Ao CDRMSP compete também a elaboração do PDUI – Plano de Desenvolvimento Urbano Integrado da RMSP. Em ato deliberativo CD-01/2015-A de janeiro de 2015, e conforme o Guia Metodológico do PDUI aprovado em 23 de maio de 2016, o CDRMSP instituiu as instâncias responsáveis pela elaboração e aprovação do PDUI.

## ID=comite

### Comitê Executivo 

Composto por 18 membros, sendo 04 representantes do Governo do Estado de São Paulo, 04 representantes da Prefeitura de São Paulo e 10 das sub-regiões que compõe a Região Metropolitana de São Paulo. O Comitê é responsável por promover a articulação e coordenação entre Estado, Municípios integrantes da região e sociedade civil com o intuito de viabilizar os trabalhos relativos à elaboração do PDUI até o momento de sua aprovação.

## ID=comissao

### Comissão Técnica 

Composta por membros representantes das 5 sub-regiões da RMSP, do Município de São Paulo e do Estado de São Paulo para promover o trabalho de elaboração do PDUI, acompanhar o seu desenvolvimento, atuando como facilitador da observância ao Estatuto da Metrópole, bem como pautar as reuniões do Comitê Executivo.

## ID=gruposSP

### Grupos de Trabalho 

Nas sub-regiões da RMSP que serão criados pela Comissão Técnica visando a mobilização dos atores regionais na apuração das demandas locais referidas aos planos municipais e regionais.

## ID=emplasa

### Empresa Paulista de Planejamento Metropolitano S.A. (Emplasa)

A Emplasa é uma empresa pública vinculada à Secretaria Estadual da Casa Civil. Criada em 1974 para planejar a Grande São Paulo, hoje dá apoio técnico às unidades regionais institucionalizadas: Regiões Metropolitanas de São Paulo, Campinas, Baixada Santista, Vale do Paraíba e Litoral Norte e Sorocaba e as Aglomerações Urbanas de Jundiaí e Piracicaba, inseridas no território denominado Macrometrópole Paulista. A empresa tem hoje como foco de suas ações o território da macrometrópole paulista e é responsável pelo planejamento regional e metropolitano do Estado de São Paulo, com a missão de elaborar e subsidiar o Governo do Estado de São Paulo na implantação de políticas públicas e projetos integrados de desenvolvimento urbano e regional. 

# RM Goiânia

## ID=secretaria

### Secretaria de Estado de Desenvolvimento e Inovação

Responsável pela formulação e execução da política estadual de ciência, tecnologia e inovação; promoção da educação profissional, tecnológica e superior; bem como pela formulação e execução da política estadual de cidades, infraestrutura e desenvolvimento da Região Metropolitana de Goiânia, especialmente nas áreas de habitação, telecomunicações, desenvolvimento urbano, transportes e obras públicas (Lei nº 20.491/2019). 

## ID=conselhoGoiania

### Conselho de Desenvolvimento Metropolitano de Goiânia – Codemetro

Compete ao Codemetro deliberar sobre a organização, planejamento e execução das funções públicas de interesse comum da RMG; monitorar e avaliar a execução do Plano de Desenvolvimento Integrado da RMG; bem como aprovar objetivos, metas, prioridades e autorizar o uso de recursos do Fundemetro.  

## ID=subsecretariaGoiania

### Subsecretaria de Assuntos Metropolitanos, Cidades, Infraestrutura, e Comércio Exterio

Responsável por coordenar e supervisionar a política estadual de desenvolvimento urbano e regional, embasada nos princípios do desenvolvimento sustentável, inclusão social e cogestão entre os poderes públicos, em articulação com órgãos e entidades das esferas federal, estadual e municipal.  

## ID=superintendênciaGoiania

### Superintendência de Políticas para Cidades e Infraestrutura

Incumbida de formular, promover e coordenar ações, programas, projetos e atividades inerentes à implementação da política estadual de desenvolvimento urbano, especialmente nas áreas de habitação, telecomunicações, desenvolvimento urbano, transportes e obras públicas. 

## ID=gerênciaGoiania

### Gerência de Programas Metropolitanos e Habitacionais

Oferecer apoio técnico ao Codemetro, mediante a realização de estudos e ações para subsidiar a execução de funções públicas de interesse comum nos municípios integrantes da região metropolitana e aglomerações urbanas; bem como formular, coordenar e propor projetos para captação de financiamento, acordos, convênios, consórcios públicos e outros instrumentos, para suporte e implementação de políticas públicas, especialmente de habitação, regularização fundiária, mobilidade e acessibilidade.   

# RM Florianópolis

## ID=suderf

### Superintendência de Desenvolvimento da Região Metropolitana da Grande Florianópolis – SUDERF

A Superintendência de Desenvolvimento da Região Metropolitana da Grande Florianópolis – SUDERF foi criada por meio da Lei Complementar n.º 636, de 09 de setembro de 2014, publicada no Diário Oficial n.º 19.898, de 10 de setembro de 2014. Trata-se de uma autarquia de regime especial, dotada de autonomia administrativa, orçamentária, financeira e patrimonial, vinculada à Secretaria de Estado do Planejamento (SPG), órgão central do Sistema de Planejamento Estratégico, a quem compete coordenar a implantação das políticas estaduais de desenvolvimento regional e urbano. A SUDERF tem sede e foro na Capital do Estado, competência no território compreendido pela Região Metropolitana da Grande Florianópolis – RMF, e tem por finalidade a consecução dos objetivos da RMF.

## ID=coderf

### Comitê de Desenvolvimento da Região Metropolitana da Grande Florianópolis - Coderf

Instância colegiada deliberativa com representação da sociedade civil. Composto por 18 (dezoito) membros e igual número de suplentes. Órgão de caráter normativo e deliberativo da RMF, se reúne 1 vez por mês. A presidência do Comitê é exercida pelo Diretor Técnico da Suderf, Existe ampla participação de entidades civis, atualmente 6 membros titulares e 6 suplentes não pertencem ao Poder público Executivo. Cada membro terá direito a 1 (um) voto.

## ID=colegio

### Colégio Superior

Órgão de instância executiva composta pelos representantes do Poder Executivo dos entes federativos integrantes das unidades territoriais urbanas: superintendente que exercerá a presidência, secretário do Estado de Desenvolvimento Regional da Grande Florianópolis, que exercerá a Vice-Presidência; Secretário de Estado do Planejamento; Secretário de Estado da Infraestrutura; e os Chefes do Poder Executivo de cada um dos Municípios que constituem a RMF. Cada membro terá direito a 1 (um) voto.

## ID=superintendencia

### Superintendência-Geral
nulo

## ID=diretoria-florianopolis

### Diretoria Administrativo-Financeira
nulo

# RM Salvador 

## ID=colegiado

### Colegiado Metropolitano

O Colegiado Metropolitano, instância máxima da Entidade Metropolitana, tem em sua composição o Governador do Estado e os Prefeitos dos treze municípios que compõem a RMS.

## ID=secretario

### Secretário Geral

O Secretário Geral da Entidade Metropolitana é o representante legal da Região Metropolitana de Salvador, cumprindo-lhe dar execução às deliberações do Colegiado. Atualmente o Secretário de Desenvolvimento Urbano do Estado da Bahia - SEDUR, pelo Regimento da Entidade, é o Secretário Geral da Entidade Metropolitana.

## ID=tecnico

### Comitê Técnico

O Comitê Técnico é composto por 03 (três) representantes do Estado da Bahia, por 03 (três) representantes do Município de Salvador e por 01 (um) representante de cada um dos demais municípios da RMS. O Comitê tem por finalidade apreciar previamente as matérias que integrem a pauta das reuniões do Colegiado Metropolitano providenciando estudos técnicos que a fundamentem e assegurem a prévia manifestação do Conselho Participativo da RMS.

## ID=conselhoSalvador

### Conselho Participativo 

O Conselho Participativo é composto por 30 (trinta) membros, sendo 01 (um) representante escolhido por cada Legislativo e os demais representantes da sociedade civil. Tem por finalidade apreciar previamente as matérias relevantes à deliberação do Colegiado Metropolitano, propor a constituição de grupo de trabalhos para a análise de temas específicos e convocar audiências e consultas públicas das matérias em questão como forma de assegurar o pluralismo e a transparência dos processos.

## ID=tematico

### Câmaras Temáticas

O Comitê Técnico criou 04 (quatro) Câmaras Temáticas para a análise de questões setoriais: Mobilidade Urbana, Transporte Metropolitano e Integração; Saneamento Básico; Habitação; e Plano Diretor de Desenvolvimento Metropolitano, também responsável pelo Uso e Ocupação do Solo. As Câmaras Temáticas são formadas por um representante de cada um dos entes federativos, e são indicados pelos respectivos membros do Comitê Técnico. As quatro Câmaras Temáticas possuem representantes do corpo técnico dos municípios e do Governo do Estado e discutem questões metropolitanas e funções públicas de interesse comum.

## ID=grupos

### Grupos de Trabalho

Tem por finalidade exercer as competências relativas à integração da organização, do planejamento e da execução das Funções Públicas de Interesse Comum aos municípios integrantes da região.

# RM Vitória

## ID=comdevit

### Conselho Metropolitano de Desenvolvimento da Grande Vitória (COMDEVIT)

A finalidade do COMDEVIT, instância de caráter deliberativo, é apoiar o desenvolvimento, a integração e a compatibilização das ações, estudos e projetos de interesse comum da RMGV. Sua composição é formada por sete representantes do governo do estado, Secretários do Poder Executivo Estadual designados pelo Governador; um representante de cada município que integra a RMGV (que se dá preferencialmente com a participação dos prefeitos) e três representantes da sociedade civil, indicados pela Federação das Associações de Moradores e dos Movimentos Populares do Estado Espírito Santo (FAMOPES). Atualmente o COMDEVIT é presidido pelo Diretor Presidente do Instituto Jones dos Santos Neves (IJSN).

## ID=cagem

### Coordenação de Apoio à Gestão Metropolitana (CAGEM)

Esse grupo de Coordenação tem como atribuição exercer as funções de Secretaria Executiva do COMDEVIT e de execução orçamentária. Para isso, a CAGEM encontra-se subordinada hierarquicamente ao Diretor Presidente do IJSN.

## ID=ijsn

### Instituto Jones dos Santos Neves (IJSN)

O instituto tem por finalidade exercer as atividades de órgão de apoio técnico, de Secretaria Executiva do COMDEVIT, no que tange à assessoria técnico-administrativa e operacionalização dos recursos orçamentários do FUMDEVIT, e de Presidência do COMDEVIT. A Lei Complementar nº 325/2005 acrescentou à estrutura organizacional básica do IJSN, em nível de execução programática, a Coordenação de Apoio à Gestão Metropolitana (CAGEM).

## ID=cates

### Câmaras Temáticas Especiais (CATES)

Com o objetivo de apresentar e debater propostas e projetos relacionados às matérias específicas do COMDEVIT, a Lei Complementar nº 318/2005, em seu artigo 9º, criou a figura das Câmaras Temáticas Especiais (CATES), que possuem caráter consultivo e devem ser constituídas por deliberação de 2/3 dos membros do COMDEVIT, de acordo com temas prioritários de interesse comum da RMGV, relacionados com os 12 campos funcionais retromencionados. As CATES devem ser compostas por no mínimo 3 e no máximo 6 membros efetivos e igual número de suplentes, assim especificados: representantes dos órgãos públicos, ligados aos campos funcionais específicos; representantes do Poder Legislativo Estadual e das Câmaras Municipais dos Municípios que compõem a RMGV, indicados pelos respectivos presidentes; e representantes da sociedade civil, incluindo-se movimentos sociais, entidades de classe, organizações empresariais, dentre outros, indicados pelos titulares das respectivas instituições.

## ID=ge

### Grupo Executivo (GE) e Grupo Técnico (GT)

O Grupo Executivo (GE) e o Grupo Técnico (GT) são instituídos por resoluções do COMDEVIT. O GE, instituído pela Resolução COMDEVIT nº 05/2007, é formado geralmente por subsecretários estaduais, secretários municipais e representantes da FAMOPES e tem responsabilidade no alinhamento das questões técnicas à perspectiva da gestão e governança metropolitana, com a interface direta do COMDEVIT. Em linhas gerais, é o GE que identifica e hierarquiza os projetos que devem ser priorizados pelo FUMDEVIT. 
O GT, instituído pela Resolução COMDEVIT 9/2007, como Grupo de Trabalho, passou a ser denominado Grupo Técnico a partir da resolução COMDEVIT nº 15/2011. É composto por técnicos indicados pelo GE, respeitando a composição do COMDEVIT (técnicos municipais, do estado e representantes indicados pela FAMOPES), e são responsáveis por desenvolver atividades específicas, de cunho técnico-operacional.

## ID=fumdevit

### Fundo Metropolitano de Desenvolvimento da Grande Vitória (FUMDEVIT)

Constitui-se um instrumento de gestão que possui o propósito de dar suporte financeiro ao planejamento integrado e às ações conjuntas de interesse comum entre o estado e os municípios integrantes da RMGV. Os recursos são aportados para projetos específicos da carteira de projetos aprovado pelo COMDEVIT, sendo o governo do estado responsável por 60% e os governos municipais por 40% dos recursos dimensionados para a realização de cada projeto.

# RM Recife

## ID=conderm

### Conselho de Desenvolvimento da Região Metropolitana da Região Metropolitana de Recife - Conderm

O Conselho de Desenvolvimento da Região Metropolitana do Recife – Conderm é um órgão deliberativo e consultivo, presidido pelo Secretário de Planejamento e Gestão de Pernambuco.  O Conderm é constituído pelos prefeitos dos 14 municípios metropolitanos e por 14 representantes do Governo do Estado, nomeados pelo Governador, como membros deliberativos.  

## ID=conselho

### Conselho Consultivo

Junto ao presidente-secretário existe um conselho composto por componentes na qualidade de membros consultivos: representantes do Poder Legislativo (municipal e estadual), sem direito a voto, sendo um parlamentar representante de cada Câmara Municipal e três deputados estaduais, representando a Assembleia Legislativa de Pernambuco.

## ID=funderm

### Condepe/Fidem

Em obediência aos dispositivos da Constituição Federal de 1967 e da Emenda Constitucional de 1968 e por determinação da Lei Federal de 1973, o Estado de Pernambuco criou através da Lei Estadual de Nº 6.873, de abril de 1975, a Fundação de Desenvolvimento da Região Metropolitana do Recife - Fidem, como órgão de apoio técnico e administrativo ao Conselho Deliberativo e Consultivo.

## ID=camaras

### Câmaras Setoriais

As Câmaras Técnicas Setoriais Metropolitanas foram criadas e regulamentadas por meio da Resolução do Conderm Nº 02/1994, presididas por um dos seus membros, escolhido em votação interna, homologada pelo Presidente do colegiado, alternando-se, anualmente a Presidência, entre representantes do poder público e da sociedade Civil. São elas: Câmara Metropolitana de Desenvolvimento Urbano e Ordenamento Territorial; Câmara Metropolitana de Transporte; Câmara Metropolitana de Meio Ambiente e Saneamento; Câmara Metropolitana de Desenvolvimento Social; Câmara Metropolitana de Política de Defesa Social.

## ID=funderm2

### Funderm

Em 1975, como instrumento financeiro, de caráter rotativo, foi instituído através da Lei Nº 7.003, de 02 de dezembro de 1975, o Fundo de Desenvolvimento da Região Metropolitana do Recife. Instrumento financeiro de caráter rotativo destina-se a financiar, total ou parcial, sob as formas de empréstimo ou a fundo as atividades de planejamento do desenvolvimento da Região Metropolitana do Recife; a gestão dos negócios relativos à Região Metropolitana do Recife; a execução das funções públicas de interesse comum; a execução e operação de serviços urbanos de interesse metropolitano.

## ID=cmds

### Câmara Metropolitana de Desenvolvimento Social (CMDS)
nulo

## ID=cmt

### Câmara Metropolitana de Transportes  (CMT)
nulo

## ID=cmduot

### Câmara Metropolitana de Des. Urbano e Ordenamento Territorial (CMDUOT) 
nulo

## ID=cmmas

### Câmara Metropolitanade Meio Ambiente e Saneamento (CMMAS)
nulo

## ID=cmpds

### Câmara Metropolitana de Política de Defesa Social (CMPDS)
nulo

# RM Porto Alegre

## ID=diretoria

### Conselho Deliberativo Metropolitano

Espaço decisório e de coordenação das políticas públicas metropolitanas, responsável pelas diretrizes para o desenvolvimento metropolitano, e tem como competências: estabelecer as diretrizes para seu desenvolvimento; planejar seu desenvolvimento estratégico; propor e aprovar o Plano Diretor da RMPA; propor e aprovar as diretrizes do Plano Plurianual para a RMPA; e identificar as ações metropolitanas prioritárias, propondo sua incorporação na Lei de Diretrizes Orçamentárias - LDO e na Lei Orçamentária Anual do Estado – LOA, bem como nas leis de diretrizes orçamentárias e leis orçamentárias anuais dos Municípios integrantes da RMPA. O Conselho é composto por dois órgãos, um propriamente deliberativo e outro executivo: Pleno e Diretoria Executiva, que ao todo é composto por 67 integrantes, entre representantes e convidados. 

## ID=representativas

### Parlamento Metropolitano 

Em abril de 2015, pela proposição da Câmara de Vereadores de Porto Alegre, foi criado na RM de Porto Alegre o Parlamento Metropolitano da Grande Porto Alegre, que reúne as 34 Câmaras de Vereadores dos municípios metropolitanos, integrando em torno de 440 vereadores. Os objetivos principais do Parlamento são: (i) promover o debate e propor a unificação de legislações municipais em temáticas urbanas comuns; (ii) incentivar a modernização dos poderes legislativos; (iii) o intercâmbio de experiências administrativas; (iv) avaliar, debater temas de interesse comuns e propor recomendações e projetos de políticas públicas integradas para a RM de Porto Alegre, por meio de Comissões.

## ID=gestao

### Fundação Estadual de Planejamento Metropolitano e Regional -Metroplan

A Metroplan foi fundada em 1975, incialmente como órgão exclusivamente de planejamento metropolitano, passando em 1999 a responder também pelo planejamento das demais aglomerações urbanas instituídas no estado a partir da década de 1990. Dessa forma, acumulou diversas atribuições ao longo de sua existência, sendo pioneira em muitas áreas, em especial na ambiental. A atuação da Metroplan está baseada em três eixos: Gestão territorial, Transportes Metropolitanos e Incentivo ao desenvolvimento.

# RM Curitiba

## ID=comec

### Conselhos Deliberativo e Consultivo

Em 1974, a Comec foi instituída pela Lei Estadual nº 6.517/1974 para a realização de serviços comuns no território metropolitano. A Comec, no âmbito do poder público estadual, articula as instâncias associadas: Secretaria Administrativa; Conselho Deliberativo; Conselho Consultivo e Conselho Gestor dos Mananciais da RM de Curitiba. O Conselho Deliberativo constituir-se-á de 5 (cinco) membros, sendo um deles indicado pelo Município de Curitiba e outros pelos demais Municípios integrantes da Região Metropolitana, todos nomeados pelo Governador do Estado. O presidente do Conselho Deliberativo é o Secretário de Estado responsável pelo planejamento estadual. O Conselho Consultivo, dirigido pelo Presidente do Conselho Deliberativo, compor-se-á de um representante de cada município integrante da região nomeado pelo Governador do Estado.

## ID=municipal

### Secretaria Municipal de Assuntos Metropolitanos (Smam)

Compete à Smam, em virtude de sua atuação como agente técnico, implementar políticas públicas de desenvolvimento no município de Curitiba, em conjunto com os demais municípios da RM de Curitiba viabilizando ações de interesse comum, por meio do assessoramento e desenvolvimento de programas e projetos para a integração regional com as seguintes atribuições: i) coordenar as ações do município com os demais municípios integrantes da RM, com o objetivo de harmonizar os assuntos de interesse comum, a integração e o desenvolvimento regional; ii) articular-se com as demais administrações municipais, entidades metropolitanas e com o governo do estado do Paraná, no processo de planejamento e gestão das Fpics, por meio de instrumento de cooperação, convênios e consórcios; iii) apoiar os demais municípios da RM de Curitiba, por meio da transferência do conhecimento técnico em programas e projetos de desenvolvimento urbano; 

## ID=assomec

### Associação dos Municípios da Região Metropolitana de Curitiba - Assomec 

A Assomec, entidade pública de caráter apartidário, congrega os 29 municípios da RM de Curitiba, representados pelos seus prefeitos. Instituída em 1982, seu orçamento é composto por verbas provenientes de contribuição (anuidade) dos municípios (prefeituras) participantes. A composição da estrutura organizacional é a seguinte: diretoria composta de presidente, dois vice-presidentes, tesoureiro e secretário; Conselho Fiscal composto por presidente, três membros titulares e três suplentes. Conselho Consultivo, do qual participam sete prefeitos. A entidade representa a totalidade dos prefeitos da região no Conselho Deliberativo da Comec.

# RM Manaus

## ID=SRMM

### Secretaria Metropolitana da Região de Manaus

Secretaria Metropolitana da Região de Manaus (SRMM) é um órgão público da esfera do Governo do Estado do Amazonas, cuja área de abrangência inclui 13 municípios: Manaus, Presidente Figueiredo, Rio Preto da Eva, Itacoatiara, Iranduba, Manacapuru, Novo Airão, Careiro da Várzea, Manaquiri, Careiro, Autazes, Itapiranga e Silves. As atribuições da SRMM incluem o planejamento e execução de programas de obras do governo estadual e financiados por órgãos internacionais.

## ID=secrtexi

### Secretaria Executiva do Conselho de Desenvovimento Sustentável

A Secretaria Executiva do Conselho de Desenvolvimento Sustentável da Região Metropolitana de Manaus - SRMM tem como finalidade a gestão das funções públicas de interesse comum da Região Metropolitana de Manaus, bem como supervisionar e fiscalizar as atividades da Unidade de Gestão Metropolitana de Manaus - UGM e  da Unidade Gestora do Programa de Desenvolvimento e Integração da Região Sul da Cidade de Manaus - UGPSUL, as quais passam a ser vinculadas a SRMM. 

## ID=UPGE

### Unidade Gestora de Projetos Especiais

Bloco de unidades administrativas dedicado à atender as demandas específicas dos projetos e programas com financiamento internacional, em particular o BID no tocante ao PROSAMIM (Programa Social e Ambiental dos Igarapés de Manaus) em seus diversos contratos.

## ID=CDSRMM

### Conselho de Desenvolvimento Sustentável

Responsável pela aprovação dos planos metropolitanos de desenvolvimento, execução de estudos, programas e projetos de interesse metropolitano, registros e controles contábeis do Fundo Especial da Região Metropolitana de Manaus – FERMM.

## ID=FERMM

### Fundo Espacial da Região Metropolitana de Manaus

O Fundo Especial da Região Metropolitana de Manaus - FERMM tem a finalidade de dar suporte financeiro ao planejamento integrado e às ações conjuntas dele decorrentes, no que se refere às funções públicas de interesse comum entre o Estado e os Municípios integrantes da Região Metropolitana de Manaus. 
