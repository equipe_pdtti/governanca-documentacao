São objetivos da Plataforma IPEA de Pesquisa em Rede:

I - implementar redes de pesquisa entre instituições de pesquisa ou representativas de pesquisadores, planejamento e de estatística, visando à integração de ações e de pesquisas em áreas temáticas definidas no planejamento estratégico do IPEA, com ênfase na aplicação de resultados focados na sustentabilidade do desenvolvimento social e econômico brasileiro;

II - viabilizar a realização de pesquisas nas áreas de economia e desenvolvimento de acordo com linhas específicas definidas pelo IPEA, em consonância com o Plano de trabalho do Instituto;

III - promover articulações institucionais que fortaleçam as instituições participantes da Rede IPEA, seja em termos de sua capacidade técnica, seja em termos dos recursos disponíveis para a realização de atividades de pesquisa;

