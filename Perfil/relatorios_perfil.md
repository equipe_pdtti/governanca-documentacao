cod=2401

**RM Natal** 

[Cap. 13 - Livro vol 4](link do bruno)

cod=1501

**RM Belém** 

[Relatório 1 (Belém)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel_1_1_rm_belem.pdf) - [Relatório 2 (Belém)-2016](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/160128_relatorio_rm_belem.pdf) 

cod=3101

**RM Belo Horizonte**

[Relatório 1 (BH)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel1_1_rmbh.pdf) - [Relatório 2 (BH)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151228_relatorio__rmbh.pdf) [Relatório 3 (BH)-2019](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190114_relatorio_de_pesquisa_implementacao_BH.pdf)

cod=4101

**RM Curitiba**

[Relatório 1 (Curitiba)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_arranjos_igm_rm_curitiba.pdf) - [Relatório 2 (Curitiba)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150930_relatorio_analise_curitiba2.pdf) 

cod=4201

**RM Florianópolis**

[Relatório 3 (Florianópolis)-2019](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190506_adequacao_de_arranjos_florianopolis.pdf)

cod=2301

**RM Fortaleza**

[Relatório 1 (Fortaleza)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150928_relatorio_arranjos_fortaleza.pdf) - [Relatório 2 (Fortaleza)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151013_relatorio_analise_fortaleza2.pdf) 

cod=2101

**RM Grande São Luís**

[Relatório 1 (São Luís)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rel1_1_rmgsl.pdf) - [Relatório 2 (São Luís)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_analise_comparativa_grande_sao_luis.pdf) 

cod=3201

**RM Grande Vitória**

[Relatório 1 (Vitória)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_arranjo_grande_vitoria.pdf) - [Relatório 2 (Vitória)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/62592_relatorio_rmgv.pdf) [Relatório 3 (Vitória)-2018](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/180219_relatorio_adequacao_dos_arranjos_de_governanca_metropolitana.pdf)

cod=5201

**RM Goiânia**

[Relatório 1 (Goiânia)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/rmg_livro_web.pdf) - [Relatório 2 (Goiânia)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150820_74657_relatorio_analise_rm_Goiania.pdf) 

cod=4301

**RM Porto Alegre**

[Relatório 1 (Porto Alegre)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_arranjos_porto_alegre.pdf) - [Relatório 2 (Porto Alegre)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_relatorio_analise_porto_alegre.pdf) [Relatório 3 (PA)-2019](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190118_relatorio_pesquisa_adequacao_porto_alegre.pdf)

cod=53

**RIDE DF** - 

[Relatório 1 (RIDE/DF)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/ride_livro_web.pdf) - [Relatório 2 (RIDE/DF)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151103_relatorio_analise_distrito_federal.pdf) 

cod=3301

**RM Rio de Janeiro**

[Relatório 1 (RJ)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150820_relatorio_arranjos_riode_janeiro.pdf) - [Relatório 2 (RJ)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150909_analise_componente2_riode_janeiro.pdf) [Relatório 3 (RJ)-2018](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150820_relatorio_arranjos_riode_janeiro.pdf)

cod=2601

**RM Recife** - 

[Relatório 1 (Recife)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150717_relatorio_arranjos_reecife.pdf) - [Relatório 2 (Recife)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150917_relatorio_analise_comparativa_rm_recife.pdf) [Relatório 3 (Recife)-2019](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/190114_relatorio_de_pesquisa_arranjo_de_governanca_recife.pdf)

cod=3501

**RM São Paulo** - 

[Relatório 1 (SP)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150730_relatorio_arranjos_saopaulo.pdf) - [Relatório 2 (SP)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151208_relatorio_analise_comparativa_rm_sao_paulo.pdf) - [Relatório 3 (SP)-2018](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/20170217_relatorio_implementacao-estatuto.pdf)

cod=2901

**RM Salvador**

[Relatório 1 (Salvador)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/relatorio_1.1_revisao_final_salvador.pdf) - [Relatório 2 (Salvador)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/151228_relatorio_analise_salvador_2.pdf) [Relatório 3 (Salvador)-2018](http://www.ipea.gov.br/portal/images/stories/PDFs/relatoriopesquisa/180223_relatorio_implementacao_salvador.pdf)

cod=5101

**RM Vale do Rio Cuiabá**

[Relatório 1 (Rio Cuiabá)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150814_relatorio_arranjos_valeriocuiba.pdf) - [Relatório 2 (Rio Cuiabá)-2015](http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/150917_relatorio_analise_vale_rio_cuiaba.pdf) 

cod=1301

**RM Manaus**

sem relatórios