

**ARTIGO** - [Artigo RBDU “Da urbanização periférica ao direito à metrópole: a Lei 13.089/2015 no reescalonamento da política urbana” – FRANZONI, J.A. e HOSHINO, T. A.P.] (http://www.ipea.gov.br/redeipea/images/pdfs/governanca_metropolitana/da_urbanizacao_periferica_ao_direito_a_m.pdf)

**LIVRO** - [LIVRO “O Estatuto da Cidade e a Habitat III : um balanço de quinze anos da política urbana no Brasil e a Nova Agenda Urbana” – ORG: COSTA, M.A.] (http://repositorio.ipea.gov.br/bitstream/11058/7121/1/O%20Estatuto%20da%20Cidade%20e%20a%20Habitat%20III.pdf)

**ARTIGO** - [Artigo ENANPUR 2017 “A Infraestrutura Urbana na berlinda: entre tímidos avanços e riscos de retrocessos” – COSTA, M.A.; MARGUTI, B. O.; FAVARÃO, C.B.; ARAGÃO, T. A.] (http://anpur.org.br/xviienanpur/principal/publicacoes/XVII.ENANPUR_Anais/ST_Sessoes_Tematicas/ST%203/ST%203.1/ST%203.1-02.pdf)

**LIVRO** - [LIVRO “Territórios em números : insumos para políticas públicas a partir da análise do IDHM e do IVS de municípios e Unidades da Federação brasileira, livro I” – ORG: MARGUTI, B.O.; COSTA, M.A.; PINTO, C.V.S] (http://repositorio.ipea.gov.br/handle/11058/7978)

**LIVRO** - [LIVRO “Territórios em números : insumos para políticas públicas a partir da análise do IDHM e do IVS de UHDs e regiões metropolitanas brasileiras, livro 2” – ORG: MARGUTI, B.O.; COSTA, M.A.; FAVARÃO, C.B.] (http://repositorio.ipea.gov.br/handle/11058/8035)

