# A Rede



![img-proj-cont-1](imgs/projeto_logoRedeIpea.png?raw=true)



O Instituto de Pesquisa Econômica Aplicada (IPEA), vinculado ao Ministério do Planejamento, Desenvolvimento e Gestão, se configura como um dos principais órgãos de pesquisa no âmbito federal e, dessa maneira, possui instrumentos e recursos que podem favorecer a implementação de articulações em rede, com uma ampla gama de atores e instituições. Considerando os desafios presentes na agenda política do país – associados à dívida social acumulada nas décadas anteriores e à necessidade de promover a redução das desigualdades regionais e as assimetrias existentes em nossa federação – a instituição estruturou, em 2012, um conjunto de articulações institucionais visando à ampliação das capacidades de investigação e conhecimento sobre a realidade brasileira. A iniciativa pretendeu contribuir para o fortalecimento do planejamento e o desenvolvimento por meio da valorização estratégica de instituições de planejamento, pesquisa e estatística, centros acadêmicos e universitários, órgãos governamentais além de organizações e entidades da sociedade civil que queiram contribuir para a execução das iniciativas de pesquisa coordenadas nacionalmente pelo IPEA.